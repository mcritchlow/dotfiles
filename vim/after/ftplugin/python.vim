setlocal tabstop=4
setlocal textwidth=79
setlocal shiftwidth=4
setlocal expandtab
setlocal autoindent
let b:undo_ftplugin .= '|setlocal tabstop< shiftwidth< expandtab< textwidth< autoindent<'
