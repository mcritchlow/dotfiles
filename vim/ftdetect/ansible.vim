" Associate ansible filetype to things in a /ops folder for a project
autocmd BufRead,BufNewFile */ops/*.yml set filetype=ansible
