#!/usr/bin/env sh

# Open images in directory in thumbnail mode

sxiv -tq * >/dev/null 2>&1
