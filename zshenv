typeset -U path
# various bins
path=(~/.bin ~/bin ~/.local/bin $path[@])

# load Go
if test -d "$HOME/go"; then
  export GOPATH="$HOME/go"
  path=(~/go/bin $path[@])
fi

# load rbenv
if test -d "$HOME/.rbenv"; then
  path=(~/.rbenv/bin ~/.rbenv/plugins/ruby-build/bin $path[@])
  eval "$(rbenv init - )"
fi

export WWW_HOME="https://duckduckgo.com"
export EDITOR=vim
export VISUAL=vim
# Lastpass
export LPASS_PINENTRY="/usr/bin/pinentry-curses"
# don't allow agent to expire
export LPASS_AGENT_TIMEOUT=0
# prefer libvirt provider for vagrant
export VAGRANT_DEFAULT_PROVIDER="libvirt"
# nnn scripts location
export NNN_SCRIPT="$HOME/.config/nnn/scripts"

# less colors
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m' # reset underline
